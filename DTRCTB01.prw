#INCLUDE "PROTHEUS.CH"
#INCLUDE "TopConn.ch"
#INCLUDE 'COLORS.CH'
#DEFINE DMPAPER_A4 9
#DEFINE ENTER chr(13)+chr(10)
/*
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±
±±ÚÄÄÄÄÄÄÄÄÄÄÂÄÄÄÄÄÄÄÄÄÄÂÄÄÄÄÄÄÄÂÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÂÄÄÄÄÄÄÂÄÄÄÄÄÄÄÄÄÄ¿±±
±±³Fun‡…o    ³DTREST01³  Autor ³ Maike Ramos de Oliveira  Data: 11/2016    ±±
±±ÃÄÄÄÄÄÄÄÄÄÄÅÄÄÄÄÄÄÄÄÄÄÁÄÄÄÄÄÄÄÁÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÁÄÄÄÄÄÄÁÄÄÄÄÄÄÄÄÄÄ´±±
±±³Descri‡…o ³Relatorio impostos a pagar por recebido        - TReport    ³±±
±±ÃÄÄÄÄÄÄÄÄÄÄÅÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ´±±
±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±
ßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßß
*/
User Function DTRCTB01()

  Local _aARelIQF  := GetArea()
  Local lDados     := .F.
  
  Private _nTotReg := 0  
  Private oReport  
  Private cPerg    := PadR ("DTRCTB01", Len (SX1->X1_GRUPO)) 
 
  CriaSx1(cPerg)
  If ! (Pergunte("DTRCTB01",.T.))
    Aviso("DTRCTB01() - Atenção" , "Cancelado pelo Usuário !" , {"Ok"} , 1 )
    RestArea(_aARelIQF)
	Return
  Endif
  FWMsgRun(,{||lDados := ProcDados()}, "Processando", "Pesquisando Dados...")
//  Processa({||lDados := ProcDados()},'Pesquisando Dados...') 
  If lDados
	  FWMsgRun(,{||oReport := ReportDef(),oReport:PrintDialog()}, "Processando", "Imprimindo Dados...") 
//    Processa({|| oReport := ReportDef(),oReport:PrintDialog()},'Imprimindo Dados...')       
  EndIf
  If Select("_QRY")>0
    _QRY->(dbCloseArea())
  Endif

  RestArea(_aARelIQF)

Return

//********************************************************************************
//Funcao      : ProcDados
//Objetivos   : Processa dados de acordo com o filtro - Criando Query do Relatorio
//Autor       : Rodrigo Teixeira
//********************************************************************************
Static Function ProcDados()

  Local cQry := ""
  If Select("_QRY") > 0
	_QRY->(DbCloseArea()) 
  Endif 
    
If MV_PAR05 == 1
    cQry := " "    
    cQry += "SELECT E1_NUM, E1_CCD, CTT_DESC01, E1_NOMCLI, E1_VALOR AS VLR_BRUTO,  "+ENTER
 	cQry += "	(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS-CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END) AS VLR_LIQUIDO ,  "+ENTER
 	cQry += "	E5_VALOR AS VLR_RECEBIDO,  "+ENTER
 	cQry += "	(E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE) AS VREC_DESCOT, "+ENTER  
 	cQry += "	ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2) AS PORCENTAGEM,   "+ENTER
 	cQry += "	ROUND(E1_VALOR*(ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2))/100,2) AS BASE_IMPOSTO,  "+ENTER
 	cQry += "	SC5.C5_XOUTRET, "+ENTER
 	cQry += "	ROUND(ROUND(E1_VALOR*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2)*0.65/100,2) AS PIS_S_FAT,  "+ENTER
 	cQry += "	ROUND(ROUND(E1_VALOR*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2)*3/100,2) AS COFINS_S_FAT,  "+ENTER
 	cQry += "	ROUND(ROUND(E1_VALOR*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2)*CTT_XALDCS/100,2) AS CSLL_S_FAT,   "+ENTER
 	cQry += "	D2_VALPIS AS VPIS_RET, "+ENTER
 	cQry += "	ROUND(D2_VALPIS*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2) as PISRET_PERC,  "+ENTER
 	cQry += "	D2_VALCOF AS VCOF_RET, "+ENTER
 	cQry += "	ROUND(D2_VALCOF*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2) as COFRET_PERC,  "+ENTER
 	cQry += "	D2_VALCSL AS VCSL_RET, "+ENTER
 	cQry += "	ROUND(D2_VALCSL*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2) as CSLRET_PERC,   "+ENTER
	cQry += "	CASE WHEN C5_RECISS = '1' THEN D2_VALISS ELSE 0 END AS VISS_RET,
	cQry += "	ROUND(CASE WHEN C5_RECISS = '1' THEN D2_VALISS ELSE 0 END*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS-CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2) as ISSRET_PERC,   
	cQry += "	CASE WHEN (SELECT A1_RECINSS FROM SA1010 WHERE A1_COD = E1_CLIENTE AND A1_LOJA = E1_LOJA AND SA1010.D_E_L_E_T_<>'*')='S' THEN D2_VALINS ELSE 0 END AS VINSS_RET,
	cQry += "	ROUND(CASE WHEN (SELECT A1_RECINSS FROM SA1010 WHERE A1_COD = E1_CLIENTE AND A1_LOJA = E1_LOJA AND SA1010.D_E_L_E_T_<>'*')='S' THEN D2_VALINS ELSE 0 END*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS-CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2) as INSSRET_PERC,   
	cQry += "	CASE WHEN (SELECT A1_RECIRRF FROM SA1010 WHERE A1_COD = E1_CLIENTE AND A1_LOJA = E1_LOJA AND SA1010.D_E_L_E_T_<>'*')='1' THEN D2_VALIRRF ELSE 0 END AS VIRRF_RET,
	cQry += "	ROUND(CASE WHEN (SELECT A1_RECIRRF FROM SA1010 WHERE A1_COD = E1_CLIENTE AND A1_LOJA = E1_LOJA AND SA1010.D_E_L_E_T_<>'*')='1' THEN D2_VALIRRF ELSE 0 END*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS-CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2) as IRRFRET_PERC,   
	cQry += "	ROUND(ROUND(ROUND(E1_VALOR*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2)*0.65/100,2) - ROUND(D2_VALPIS*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2),2) AS PISPAGAR,  "+ENTER
	cQry += "	ROUND(ROUND(ROUND(E1_VALOR*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2)*3/100,2) - ROUND(D2_VALCOF*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2),2)AS COFPAGAR,  "+ENTER
	cQry += "	ROUND(ROUND(ROUND(E1_VALOR*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2)*CTT_XALDCS/100,2) - ROUND(D2_VALCSL*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2),2) AS CSLPAGAR  "+ENTER
	cQry += "FROM SE5010 AS SE5 LEFT JOIN SE1010 AS SE1 ON SE1.E1_FILIAL = SE5.E5_FILIAL AND SE1.E1_PREFIXO = SE5.E5_PREFIXO AND SE1.E1_NUM = SE5.E5_NUMERO AND E1_CLIENTE = E5_CLIFOR AND E1_LOJA = E5_LOJA AND SE1.E1_TIPO = 'NF' AND SE1.D_E_L_E_T_<> '*'   "+ENTER
	cQry += "LEFT JOIN SD2010 AS SD2 ON SD2.D2_FILIAL = SE5.E5_FILIAL AND SD2.D2_DOC = SE5.E5_NUMERO AND SD2.D2_SERIE = SE5.E5_PREFIXO AND SD2.D_E_L_E_T_<> '*'   "+ENTER
	cQry += "LEFT JOIN CTT010 AS CTT ON SE1.E1_CCD = CTT_CUSTO AND CTT.D_E_L_E_T_<> '*'  "+ENTER
	cQry += "LEFT JOIN SC5010 AS SC5 ON SC5.C5_FILIAL = SE5.E5_FILIAL AND SC5.C5_NOTA = SE5.E5_NUMERO AND SC5.C5_SERIE = SE5.E5_PREFIXO AND SC5.C5_CLIENTE = SE5.E5_CLIFOR AND SC5.C5_LOJACLI = SE5.E5_LOJA AND SC5.D_E_L_E_T_<> '*'  "+ENTER
	cQry += "WHERE E5_FILIAL = '"+xFilial("SE5")+"' "+ENTER
    cQry += "AND E5_PREFIXO = '9' "+ENTER
    cQry += "AND E5_DTDIGIT BETWEEN '"+DtoS(MV_PAR01)+"' AND '"+DtoS(MV_PAR02)+"' "+ENTER
    cQry += "AND E1_CCD BETWEEN '"+MV_PAR03+"' AND '"+MV_PAR04+"' "+ENTER
    cQry += "AND E5_TIPODOC NOT IN ('DC','MT','CM','JR') "+ENTER
    cQry += "AND SE5.D_E_L_E_T_<> '*' "+ENTER
    cQry += "ORDER BY E1_CCD,E1_NUM "+ENTER

Else
	cQry := " "      
	cQry := " SELECT REL.E1_CCD,REL.CTT_DESC01, REL.E1_NOMCLI, SUM(REL.VLR_BRUTO) AS VLR_BRUTO, SUM(REL.VLR_LIQUIDO) AS VLR_LIQUIDO, SUM(REL.VLR_RECEBIDO) AS VLR_RECEBIDO, "+ENTER
    cQry += "	SUM(REL.VREC_DESCOT) AS VREC_DESCOT, SUM(REL.PORCENTAGEM) AS PORCENTAGEM, SUM(REL.BASE_IMPOSTO) AS BASE_IMPOSTO, SUM(REL.PIS_S_FAT) AS PIS_S_FAT, "+ENTER 
    cQry += "	SUM(REL.COFINS_S_FAT) AS COFINS_S_FAT, SUM(REL.CSLL_S_FAT) AS CSLL_S_FAT, SUM(REL.VPIS_RET) AS VPIS_RET, SUM(REL.VCOF_RET) AS VCOF_RET, SUM(REL.VCSL_RET) AS VCSL_RET, "+ENTER
    cQry += "	SUM(REL.PISRET_PERC) AS PISRET_PERC, SUM(REL.COFRET_PERC) AS COFRET_PERC, SUM(REL.CSLRET_PERC) AS CSLRET_PERC,SUM(REL.VISS_RET) VISS_RET, SUM(REL.ISSRET_PERC) ISSRET_PERC,"+ENTER
    cQry += "	SUM(REL.VINSS_RET) VINSS_RET, SUM(REL.INSSRET_PERC) INSSRET_PERC, SUM(REL.VIRRF_RET) VIRRF_RET,SUM(REL.IRRFRET_PERC) IRRFRET_PERC,SUM(REL.PISPAGAR) AS PISPAGAR, "+ENTER
    cQry += "   SUM(REL.COFPAGAR) AS COFPAGAR, SUM(REL.CSLPAGAR) AS CSLPAGAR "+ENTER	  
    cQry += " FROM ( "+ENTER
    cQry += "SELECT E1_NUM, E1_CCD, CTT_DESC01, E1_NOMCLI, E1_VALOR AS VLR_BRUTO,  "+ENTER
 	cQry += "	(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS-CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END) AS VLR_LIQUIDO ,  "+ENTER
 	cQry += "	E5_VALOR AS VLR_RECEBIDO,  "+ENTER
 	cQry += "	(E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE) AS VREC_DESCOT, "+ENTER  
 	cQry += "	ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2) AS PORCENTAGEM,   "+ENTER
 	cQry += "	ROUND(E1_VALOR*(ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2))/100,2) AS BASE_IMPOSTO,  "+ENTER
 	cQry += "	SC5.C5_XOUTRET, "+ENTER
 	cQry += "	ROUND(ROUND(E1_VALOR*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2)*0.65/100,2) AS PIS_S_FAT,  "+ENTER
 	cQry += "	ROUND(ROUND(E1_VALOR*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2)*3/100,2) AS COFINS_S_FAT,  "+ENTER
 	cQry += "	ROUND(ROUND(E1_VALOR*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2)*CTT_XALDCS/100,2) AS CSLL_S_FAT,   "+ENTER
 	cQry += "	D2_VALPIS AS VPIS_RET, "+ENTER
 	cQry += "	ROUND(D2_VALPIS*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2) as PISRET_PERC,  "+ENTER
 	cQry += "	D2_VALCOF AS VCOF_RET, "+ENTER
 	cQry += "	ROUND(D2_VALCOF*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2) as COFRET_PERC,  "+ENTER
 	cQry += "	D2_VALCSL AS VCSL_RET, "+ENTER
 	cQry += "	ROUND(D2_VALCSL*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2) as CSLRET_PERC,   "+ENTER
	cQry += "	CASE WHEN C5_RECISS = '1' THEN D2_VALISS ELSE 0 END AS VISS_RET,
	cQry += "	ROUND(CASE WHEN C5_RECISS = '1' THEN D2_VALISS ELSE 0 END*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS-CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2) as ISSRET_PERC,   
	cQry += "	CASE WHEN (SELECT A1_RECINSS FROM SA1010 WHERE A1_COD = E1_CLIENTE AND A1_LOJA = E1_LOJA AND SA1010.D_E_L_E_T_<>'*')='S' THEN D2_VALINS ELSE 0 END AS VINSS_RET,
	cQry += "	ROUND(CASE WHEN (SELECT A1_RECINSS FROM SA1010 WHERE A1_COD = E1_CLIENTE AND A1_LOJA = E1_LOJA AND SA1010.D_E_L_E_T_<>'*')='S' THEN D2_VALINS ELSE 0 END*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS-CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2) as INSSRET_PERC,   
	cQry += "	CASE WHEN (SELECT A1_RECIRRF FROM SA1010 WHERE A1_COD = E1_CLIENTE AND A1_LOJA = E1_LOJA AND SA1010.D_E_L_E_T_<>'*')='1' THEN D2_VALIRRF ELSE 0 END AS VIRRF_RET,
	cQry += "	ROUND(CASE WHEN (SELECT A1_RECIRRF FROM SA1010 WHERE A1_COD = E1_CLIENTE AND A1_LOJA = E1_LOJA AND SA1010.D_E_L_E_T_<>'*')='1' THEN D2_VALIRRF ELSE 0 END*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS-CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2) as IRRFRET_PERC,   
	cQry += "	ROUND(ROUND(ROUND(E1_VALOR*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2)*0.65/100,2) - ROUND(D2_VALPIS*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2),2) AS PISPAGAR,  "+ENTER
	cQry += "	ROUND(ROUND(ROUND(E1_VALOR*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2)*3/100,2) - ROUND(D2_VALCOF*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2),2)AS COFPAGAR,  "+ENTER
	cQry += "	ROUND(ROUND(ROUND(E1_VALOR*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2)*CTT_XALDCS/100,2) - ROUND(D2_VALCSL*ROUND((E5_VALOR + E5_VLDESCO-E5_VLJUROS-E5_VLMULTA-E5_VLCORRE)/(E1_VALOR -E1_IRRF - E1_INSS - E1_CSLL - E1_COFINS - E1_PIS  -CASE WHEN C5_RECISS = '2' THEN 0 ELSE E1_ISS END)*100,2)/100,2),2) AS CSLPAGAR  "+ENTER
	cQry += "FROM SE5010 AS SE5 LEFT JOIN SE1010 AS SE1 ON SE1.E1_FILIAL = SE5.E5_FILIAL AND SE1.E1_PREFIXO = SE5.E5_PREFIXO AND SE1.E1_NUM = SE5.E5_NUMERO AND E1_CLIENTE = E5_CLIFOR AND E1_LOJA = E5_LOJA AND SE1.E1_TIPO = 'NF' AND SE1.D_E_L_E_T_<> '*'   "+ENTER
	cQry += "LEFT JOIN SD2010 AS SD2 ON SD2.D2_FILIAL = SE5.E5_FILIAL AND SD2.D2_DOC = SE5.E5_NUMERO AND SD2.D2_SERIE = SE5.E5_PREFIXO AND SD2.D_E_L_E_T_<> '*'   "+ENTER
	cQry += "LEFT JOIN CTT010 AS CTT ON SE1.E1_CCD = CTT_CUSTO AND CTT.D_E_L_E_T_<> '*'  "+ENTER
	cQry += "LEFT JOIN SC5010 AS SC5 ON SC5.C5_FILIAL = SE5.E5_FILIAL AND SC5.C5_NOTA = SE5.E5_NUMERO AND SC5.C5_SERIE = SE5.E5_PREFIXO AND SC5.C5_CLIENTE = SE5.E5_CLIFOR AND SC5.C5_LOJACLI = SE5.E5_LOJA AND SC5.D_E_L_E_T_<> '*'  "+ENTER
	cQry += "WHERE E5_FILIAL = '"+xFilial("SE5")+"' "+ENTER
    cQry += "AND E5_PREFIXO = '9' "+ENTER
    cQry += "AND E5_DTDIGIT BETWEEN '"+DtoS(MV_PAR01)+"' AND '"+DtoS(MV_PAR02)+"' "+ENTER
    cQry += "AND E1_CCD BETWEEN '"+MV_PAR03+"' AND '"+MV_PAR04+"' "+ENTER
    cQry += "AND E5_TIPODOC NOT IN ('DC','MT','CM','JR') "+ENTER
    cQry += "AND SE5.D_E_L_E_T_<> '*' "+ENTER
    cQry += " ) REL "+ENTER
    cQry += " GROUP BY REL.E1_CCD, REL.E1_NOMCLI,REL.CTT_DESC01 "+ENTER
EndIf

// MemoWrite("C:\Utilitarios\Qry_imposto_sobre_recebimento.SQL", cQry)  
  PlsQuery(cQry,"_QRY")
  If _QRY->(EOF()) .AND. _QRY->(BOF())
     MsgAlert("Informações Não Encontraas")
     Return .F.
  EndIf

  _QRY->(dbGoTop())
  _nTotReg := 0
  While _QRY->(!EOF())
    _nTotReg++
    _QRY->(DBSKIP())
  Enddo
  _QRY->(dbGoTop())
 
Return .T.

//***********************************************
//Funcao      : ReportDef
//Objetivos   : Define estrutura de impressão
//Autor       : Rodrigo Teixeira
//***********************************************
Static Function ReportDef()
  Local oCel
  oReport := TReport():New(FUNNAME(),"Análise de Imposto sobre recebimento" , "DTRCTB01" , {|oReport| ReportPrint(oReport)},"Este relatorio irá Imprimir a apuraçao de imposto sobre recebimento")

  //Inicia o relatório como retrato. 
  oReport:oPage:lLandScape := .T.   //Paisagem
  oReport:oPage:lPortRait  := .F.   //Retrato    
  oReport:SetLineHeight(30)         //Altura da Linha
  oReport:SetColSpace(1)           
  oReport:SetLeftMargin(0)          //Tamanho da margem esquerda
  oReport:oPage:SetPageNumber(1)
  oReport:cFontBody      := 'Courier New'
  oReport:nFontBody      := 6
  oReport:lBold          := .F.
  oReport:lUnderLine     := .F.
  oReport:lHeaderVisible := .T.
  oReport:lFooterVisible := .T.
  oReport:lParamPage     := .F.     
  oReport:oPage:setPaperSize(9)
  
  //Define seções do relatório
  Private oSection1 := TRSection():New(oReport,"Movimento" , "_QRY" , {} )

    //Dados do Movimento
    If MV_PAR05 == 1
	    oReport:lBold          := .T.
	    TRCell():New(oSection1,"E1_NUM"			, "_QRY", "Nota Fiscal"  	, AVSX3("E1_NUM",   6) , 10 , , , "LEFT",   .F.) 
	    TRCell():New(oSection1,"E1_NOMCLI"		, "_QRY", "Cliente"  		, AVSX3("E1_NOMCLI",   6) , 40 , , , "LEFT",  .F.) 
	    TRCell():New(oSection1,"CTT_DESC01"		, "_QRY", "Desc. CC"  		, AVSX3("CTT_DESC01",   6) , 40 , , , "LEFT",  .F.)
	    TRCell():New(oSection1,"E1_CCD"			, "_QRY", "Centro de Custo" , AVSX3("E1_CCD",   6) , 06 , , , "LEFT",   .F.) 
	    TRCell():New(oSection1,"VLR_BRUTO"		, "_QRY", "Valor Bruto"		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"VLR_LIQUIDO"	, "_QRY", "Liq.+Caucao"		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"VLR_RECEBIDO"	, "_QRY", "Valor Recebido"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"VREC_DESCOT"	, "_QRY", "Recebido+Desconto", AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.) 
	    TRCell():New(oSection1,"PORCENTAGEM"	, "_QRY", "% Sob.Recebido"	, "@E 999.99" , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"BASE_IMPOSTO"	, "_QRY", "Base Imposto"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.) 
	    TRCell():New(oSection1,"PIS_S_FAT"		, "_QRY", "PIS s/ Fat."		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.) 
	    TRCell():New(oSection1,"COFINS_S_FAT"	, "_QRY", "COF s/ Fat."		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"CSLL_S_FAT"		, "_QRY", "CSLL s/ Fat."	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"VPIS_RET"		, "_QRY", "PIS Retido"		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
  	    TRCell():New(oSection1,"PISRET_PERC"	, "_QRY", "PIS Retido %"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.) 
	    TRCell():New(oSection1,"VCOF_RET"		, "_QRY", "COFINS Retido"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"COFRET_PERC"	, "_QRY", "COFINS Retido %"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.) 
	    TRCell():New(oSection1,"VCSL_RET"		, "_QRY", "CSLL Retido"		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"CSLRET_PERC"	, "_QRY", "CSLL Retido %"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"VISS_RET"		, "_QRY", "ISS Retido"		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"ISSRET_PERC"	, "_QRY", "ISS Retido %"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"VINSS_RET"		, "_QRY", "INSS Retido"		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"INSSRET_PERC"	, "_QRY", "INSS Retido %"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"VIRRF_RET" 		, "_QRY", "IRRF Retido"		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"IRRFRET_PERC "	, "_QRY", "IRRF Retido %"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"PISPAGAR"		, "_QRY", "Valor Pagar PIS"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)  
	    TRCell():New(oSection1,"COFPAGAR"		, "_QRY", "Valor Pagar COF"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"CSLPAGAR"		, "_QRY", "Valor Pagar CSL"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    oReport:lBold          := .F. 
	EndIF
	If MV_PAR05 == 2
		 oReport:lBold          := .T.
	    TRCell():New(oSection1,"E1_NOMCLI"		, "_QRY", "Cliente"  		, AVSX3("E1_NOMCLI",   6) , 40 , , , "LEFT",  .F.) 
	    TRCell():New(oSection1,"E1_CCD"			, "_QRY", "Centro de Custo" , AVSX3("E1_CCD",   6) , 06 , , , "LEFT",   .F.) 
	    TRCell():New(oSection1,"CTT_DESC01"		, "_QRY", "Desc. CC"  		, AVSX3("CTT_DESC01",   6) , 40 , , , "LEFT",  .F.)
	    TRCell():New(oSection1,"VLR_BRUTO"		, "_QRY", "Valor Bruto"		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"VLR_LIQUIDO"	, "_QRY", "Liq.+Caucao"		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"VLR_RECEBIDO"	, "_QRY", "Valor Recebido"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"VREC_DESCOT"	, "_QRY", "Recebido+Desconto", AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.) 
	    TRCell():New(oSection1,"BASE_IMPOSTO"	, "_QRY", "Base Imposto"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.) 
	    TRCell():New(oSection1,"PIS_S_FAT"		, "_QRY", "PIS s/ Fat."		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.) 
	    TRCell():New(oSection1,"COFINS_S_FAT"	, "_QRY", "COF s/ Fat."		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"CSLL_S_FAT"		, "_QRY", "CSLL s/ Fat."	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"VPIS_RET"		, "_QRY", "PIS Retido"		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
  	    TRCell():New(oSection1,"PISRET_PERC"	, "_QRY", "PIS Retido %"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.) 
	    TRCell():New(oSection1,"VCOF_RET"		, "_QRY", "COFINS Retido"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"COFRET_PERC"	, "_QRY", "COFINS Retido %"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.) 
	    TRCell():New(oSection1,"VCSL_RET"		, "_QRY", "CSLL Retido"		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"CSLRET_PERC"	, "_QRY", "CSLL Retido %"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"VISS_RET"		, "_QRY", "ISS Retido"		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"ISSRET_PERC"	, "_QRY", "ISS Retido %"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"VINSS_RET"		, "_QRY", "INSS Retido"		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"INSSRET_PERC"	, "_QRY", "INSS Retido %"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"VIRRF_RET" 		, "_QRY", "IRRF Retido"		, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"IRRFRET_PERC "	, "_QRY", "IRRF Retido %"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)	     
	    TRCell():New(oSection1,"PISPAGAR"		, "_QRY", "Valor Pagar PIS"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)  
	    TRCell():New(oSection1,"COFPAGAR"		, "_QRY", "Valor Pagar COF"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    TRCell():New(oSection1,"CSLPAGAR"		, "_QRY", "Valor Pagar CSL"	, AVSX3("E5_VALOR", 6) , 10 , , , "LEFT",   .F.)
	    oReport:lBold          := .F. 
	EndIf
  

Return oReport

//***************************************************
//Funcao      : ReportPrint
//Objetivos   : Imprime os dados filtrados
//Autor       : Rodrigo Teixeira
//***************************************************
Static Function ReportPrint(oReport)
  Local _cProd   := ""
  Local _cArm   := ""
  Local _lCabeca := .T.
  
  //Inicio da impressão da seção.
  oReport:Section("Movimento"):Init()
  oReport:SetMeter(_nTotReg)

  //Inicio dos Dados gerados pela Query
  _QRY->(DBGOTOP()) 
  
  Do While _QRY->(!EoF()) .And. ! oReport:Cancel()
   

      oReport:Section("Movimento"):PrintLine() //Impressão da linha
     oReport:ThinLine()

    oReport:IncMeter(1)//incrementa a barra de progressão
    
    _QRY->(DBSkip())
     

  EndDo

  //Fim da impressão da seção 
  oReport:Section("Movimento"):Finish()         

Return .T.
//********************************************
// Criação das Perguntas
//********************************************
Static Function CriaSX1(cPerg)

  Local aAreaSX1 := GetArea() 
  Local aHelpPor := {}
  Local aHelpEsp := {}
  Local aHelpIng := {}
  
  // Incluindo Help Pergunta
  aHelpPor := {}
  Aadd( aHelpPor, "Intervalo inicial de Data" )  
  aHelpIng := aHelpEsp := aHelpPor 
  PutSx1(cPerg,"01","Data Inicial ","","","MV_CH1","D",08,0,0,"G","","","","","MV_PAR01","","","","","","","","","","","","","","","","",aHelpPor,aHelpIng,aHelpEsp )

  // Incluindo Help Pergunta
  aHelpPor := {}
  Aadd( aHelpPor, "Intervalo final de Data" )  
  aHelpIng := aHelpEsp := aHelpPor 
  PutSx1(cPerg,"02","Data Final ","","","MV_CH2","D",08,0,0,"G","","","","","MV_PAR02","","","","","","","","","","","","","","","","",aHelpPor,aHelpIng,aHelpEsp )

  // Incluindo Help Pergunta
  aHelpPor := {}
  Aadd( aHelpPor, "Intervalo inicial CC" )  
  aHelpIng := aHelpEsp := aHelpPor 
  PutSx1(cPerg,"03","Centro de Custo de? ","","","MV_CH3","C",TAMSX3("CTT_CUSTO")[1],0,0,"G","","CTT","","","MV_PAR03","","","","","","","","","","","","","","","","",aHelpPor,aHelpIng,aHelpEsp )

  // Incluindo Help Pergunta
  aHelpPor := {}
  Aadd( aHelpPor, "Intervalo final CC" )  
  aHelpIng := aHelpEsp := aHelpPor 
  PutSx1(cPerg,"04","Centro de Custo ate? ","","","MV_CH4","C",TAMSX3("CTT_CUSTO")[1],0,0,"G","","CTT","","","MV_PAR04","","","","","","","","","","","","","","","","",aHelpPor,aHelpIng,aHelpEsp )

  // Incluindo Help Pergunta
  aHelpPor := {}
  Aadd( aHelpPor, "Tipo de Impressão" )  
  aHelpIng := aHelpEsp := aHelpPor 
  putSx1(cPerg,"05","Tipo de Impressao?","","","MV_CH5","N",01,0,0,"C","","","","","MV_PAR05","Analitico","Analitico","Analitico","","Sintetico","Sintetico","Sintetico", "","","","","","","","","",aHelpPor,aHelpIng,aHelpEsp )

  RestArea(aAreaSX1)

Return
//********************************************