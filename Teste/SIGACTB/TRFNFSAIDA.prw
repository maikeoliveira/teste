#INCLUDE "PROTHEUS.CH"
#include "TOTVS.CH"
#INCLUDE 'RWMAKE.CH'
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'
#include "topconn.ch"
#include "TbiConn.ch" 
#Include "MSGRAPHI.CH"

/*
±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±?
±±ÉÍÍÍÍÍÍÍÍÍÍÑÍÍÍÍÍÍÍÍÍÍËÍÍÍÍÍÍÍÑÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍËÍÍÍÍÍÍÑÍÍÍÍÍÍÍÍÍÍÍÍÍ»±?
±±ºPrograma  TRFNFSAIDA	    ºAutor ³Maike Ramos      ?Data ? 17/08/16     º±?
±±ÌÍÍÍÍÍÍÍÍÍÍØÍÍÍÍÍÍÍÍÍÍÊÍÍÍÍÍÍÍÏÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÊÍÍÍÍÍÍÏÍÍÍÍÍÍÍÍÍÍÍÍÍ¹±?
±±ºDesc.    ? Rotina Customizada para geração da NF de remessao da        º±?
±±?         ? Filial Matriz para Galpão movendo todo estoque              º±?
±±ÌÍÍÍÍÍÍÍÍÍÍØÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ¹±?
±±ºUso       ?DataTraffic                                                 º±?
±±ÈÍÍÍÍÍÍÍÍÍÍÏÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ¼±?
±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±±?
ßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßßß?
*/

User Function TRFNFSAIDA()
Local oDlg	
Local aSize    		:= MsAdvSize()
Local nOpca    		:= 0
Local bOk 			:= {||nOpca:=1, oDlg:End()}
Local bCancel		:= {||nOpca:=2, oDlg:End()}
Local oPanel1
Local oSay1    
Local oBrowse     
Local nItemSaida 	:= 0
Local _nParcial 	:= 0
Local _TotalReg		:= 0
Local aSlinha		:= {}
Local aSitens		:= {}
Local aScabec		:= {}
Local cCC			:= GetMV("MV_XCCEST")  
Local cItemC		:= GetMV("MV_XITEMC")
Private lReadOnly 	:= .T.
Private _cQuery
Private _cQuery2
PRIVATE lMsErroAuto := .F.
Private oFont   	:= TFont():New('Arial',,-12,.T.,.F.,,,,,,.F.)
Private _FL     	:= CHR(13)+CHR(10) //variavel para pular linha
Private cPerg   	:= "PTRFSAIDA"  
Private cTesOrig	:= "" 
Private cTesTrf		:= ""
Private cCfOrig		:= ""
Private lContinua    
Private cSerie 		:= "001"

//Chama criaçãoo da Pergunta
CriaSx1('PTRFSAIDA')

//Processo cancelado pelo Usuário
If ! (Pergunte('PTRFSAIDA',.T.))
	Aviso("PTRFSAIDA() - Atenção" , "Cancelado pelo Usuário !" , {"Ok"} , 2 )
	Return
Endif


cTesOrig		:= MV_PAR01
cCfOrig	   		:= POSICIONE("SF4", 1, xFilial("SF4") + cTesOrig, "F4_CF") //Posiciona codigo fiscal de acordo com a TES origem e filial logada
cTesTrf			:= POSICIONE("SF4", 1, xFilial("SF4") + cTesOrig, "F4_XTRF")

If select("QRY")>0
   QRY->(dbCloseArea())
Endif
 

_cQuery := " SELECT TOP 50 B1_COD,B1_UM,B1_DESC, B2_COD, B2_LOCAL, B2_CM1,B2_QATU, (B2_CM1*B2_QATU) AS B2_TOTAL FROM  "+RetSqlName("SB1")+" AS SB1,  "+RetSqlName("SB2")+" AS SB2	"+_FL 
_cQuery += " WHERE B1_MSBLQL = '2'	"+_FL 
_cQuery += " AND B1_XLOCAMZ = '2'	"+_FL 
_cQuery += " AND B1_GRUPO <> 'ET'	"+_FL 
_cQuery += " AND B2_FILIAL = '0101'	"+_FL 
_cQuery += " AND B1_COD = B2_COD	"+_FL 
_cQuery += " AND B1_COD BETWEEN '"+MV_PAR02+"' AND '"+MV_PAR03+"'	"+_FL 
_cQuery += " AND B2_QATU <> 0	"+_FL 
_cQuery += " AND SB1.D_E_L_E_T_<>'*'	"+_FL 
_cQuery += " AND SB2.D_E_L_E_T_<>'*'	"+_FL 
_cQuery += " ORDER BY B2_COD	"+_FL 
MemoWrite("C:\temp\Qry_Transf_todo_estoque.SQL", _cQuery)  
TCQUERY _cQuery NEW ALIAS "QRY"
DbSelectArea("QRY")
QRY->(dbGoTop())

If select("QRY2")>0
   QRY2->(dbCloseArea())
Endif 	
 
//CONSULTA CLIENTE GALPÃO
_cQuery2 := " SELECT SA1.A1_COD, SA1.A1_LOJA, SA1.A1_NOME FROM "+RetSqlName("SA1")+" AS SA1 "+_FL 
_cQuery2 += " WHERE SA1.A1_FILTRF = '0103' "+_FL //configurar campo no cliente que irá aparecer na nota de saida
_cQuery2 += " AND SA1.D_E_L_E_T_<>'*'"+_FL 

MemoWrite("C:\temp\Qry_Cliente.SQL", _cQuery2)  
TCQUERY _cQuery2 NEW ALIAS "QRY2"
DbSelectArea("QRY2")
QRY2->(dbGoTop())


If !EMPTY(QRY->B2_COD)
   DEFINE MSDIALOG oDlg TITLE " TRANSFERENCIA DO ESTOQUE 30 ITENS" STYLE DS_MODALFRAME FROM aSize[7],0 To aSize[6],aSize[5] of oMainWnd PIXEL 

  oPanel := TPanel():New(00,00,"",oDlg,oFont,.T.,,,,aSize[3],aSize[4])
  oBrowse:= TCBrowse():New( 1,1, aSize[3]-3,aSize[4]-3,,, ,oDlg,,,,{||},{||},,,,,,,.F.,"QRY",.T.,,.F.,,.T.,.T.)
//  oBrowse := BrGetDDB():new( 1,1, aSize[3]-3,aSize[4]-3,,,,oDlg,,,,,,,,,,,,.F.,'QRY',.T.,,.F.,,, ) 
//  oBrowse:AddColumn(TCColumn():New('Item'			,{||QRY->D1_ITEM }	,                   	,,,'LEFT' ,,.F.,.F.,,,,.F.,))
  oBrowse:AddColumn(TCColumn():New('Produto'      	,{||QRY->B1_COD}    ,                   	,,,'LEFT' ,,.F.,.F.,,,,.F.,))
  oBrowse:AddColumn(TCColumn():New('Descrição'    	,{||QRY->B1_DESC},                   	,,,'LEFT' ,,.F.,.F.,,,,.F.,))
  oBrowse:AddColumn(TCColumn():New('Quantidade'     ,{||QRY->B2_QATU}  ,                   	,,,'LEFT' ,,.F.,.F.,,,,.F.,))
  oBrowse:AddColumn(TCColumn():New('Vlr.Unitario'   ,{||QRY->B2_CM1}  ,                   	,,,'LEFT' ,,.F.,.F.,,,,.F.,))
  oBrowse:AddColumn(TCColumn():New('Vlr.Total' 		,{||ROUND(QRY->B2_TOTAL,2)}  ,                   	,,,'LEFT' ,,.F.,.F.,,,,.F.,))
  oBrowse:SetDisable(.T.)		
    ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg, bOk, bCancel) CENTERED
else
	Aviso("PTRANSF() - Atenção" , "Nenhum produto pode ser transferido desse Documento. Verificar se TES movimenta estoque ou Produto para armazenagem no galpão," , {"Ok"} , 2 )
	Return
EndIf
If nOpca == 2
	Aviso("PTRANSF() - Atenção" , "Cancelado pelo Usuário !" , {"Ok"} , 2 )
	Return
Endif
If nOpca == 1
		/*########################## Monta array com cabeçalho #############################*/ 
		
	cDoc := GetSxeNum("SC5","C5_NUM")		
	RollBAckSx8()		
	
	aadd(aScabec,{"C5_FILIAL"   ,"0101",Nil})	
	aadd(aScabec,{"C5_NUM"   ,cDoc,Nil})		
	aadd(aScabec,{"C5_TIPO" ,"N",Nil})		
	aadd(aScabec,{"C5_CLIENTE",QRY2->A1_COD,Nil})
	aadd(aScabec,{"C5_XNOMCLI",QRY2->A1_NOME,Nil})		
	aadd(aScabec,{"C5_LOJACLI",QRY2->A1_LOJA,Nil})		
	aadd(aScabec,{"C5_LOJAENT",QRY2->A1_LOJA,Nil})		
	aadd(aScabec,{"C5_XCC",cCC,Nil})		
	aadd(aScabec,{"C5_XITEMC",cItemC,Nil})		
	aadd(aScabec,{"C5_XDTINIM",Date(),Nil})		
	aadd(aScabec,{"C5_XDTFINM",Date(),Nil})		
	aadd(aScabec,{"C5_CONDPAG","000",Nil})
	aadd(aScabec,{"C5_TIPOCLI","F",Nil})	
	
	QRY->(dbGoTop()) 
	If 	QRY->( ! EOF())
		While QRY->( ! EOF()) 
		_TotalReg++  //totalizador 
		QRY->(DBSkip())
		Enddo
	endif
	QRY->(dbGoTop())	
	If 	QRY->( ! EOF())
		While QRY->( ! EOF())
			nItemSaida++
			_nParcial++
			IncProc("Processando Doc.Saida: "+cDoc+ " - Registro: "+CvalToCHAR(_nParcial)+"/"+CvalToCHAR(_TotalReg))
			aSlinha :={}
			
			//Monta array com itens da Nota de Saida
			aadd(aSlinha,{"C6_FILIAL"   ,"0101",Nil})
			aadd(aSlinha,{"C6_ITEM"		,StrZero(nItemSaida,Len(SC6->C6_ITEM)),Nil})			
			aadd(aSlinha,{"C6_PRODUTO"	,QRY->B1_COD,Nil})			
			aadd(aSlinha,{"C6_DESCRI"	,QRY->B1_DESC,Nil})			
			aadd(aSlinha,{"C6_UM"		,QRY->B1_UM,Nil})			
			aadd(aSlinha,{"C6_QTDVEN"	,QRY->B2_QATU,Nil})			
			aadd(aSlinha,{"C6_PRCVEN"	,QRY->B2_CM1,Nil})			
			aadd(aSlinha,{"C6_PRUNIT"	,0,Nil})			
			aadd(aSlinha,{"C6_VALOR"	,ROUND(QRY->B2_TOTAL,2),Nil})
			aadd(aSlinha,{"C6_TES"		,cTesOrig,Nil})			
			aadd(aSlinha,{"C6_LOCAL"	,QRY->B2_LOCAL,Nil})	
			aadd(aSlinha,{"C6_CF"		,cCfOrig,Nil})	
			aadd(aSlinha,{"C6_CLI"		,QRY2->A1_COD,Nil})	
			aadd(aSlinha,{"C6_LOJA"		,QRY2->A1_LOJA,Nil})	
			aadd(aSlinha,{"C6_ENTREG"	,Date(),Nil})	
			aadd(aSlinha,{"C6_DATFAT"	,Date(),Nil})	
			aadd(aSlinha,{"C6_CLASFIS"	,"040",Nil})	//CST INFORMADA PELA CONTABILIDADE
			
			aadd(aSitens,aSlinha)
			QRY->(DBSkip())			
		EndDo
		
		MATA410(aScabec,aSitens,3)
	
		If !lMsErroAuto			
			ConOut("Incluido Doc. de Saida! ")		
		Else			
			ConOut("Erro na inclusao Doc. de Saida!") 
			MostraErro()
			Return		
		EndIf 
		
		lContinua := Sx5NumNota(@cSerie,SuperGetMV("MV_TPNRNFS"))
		If lContinua 
	   		Processa({|| GeraNF(cDoc)},"Aguarde","Gerando Nota Fiscal...")
		Else 
		   Aviso("DTFTRFIL() - Atenção" , "Pedido de venda "+cDoc+" não foi processado por não ter selecionado o numero de RPS, transmitir manualmente." , {"Ok"} , 2 )
		   Return
		Endif	
		

	else
		Aviso("DTFTRFIL() - Atenção" , "Processo interrompido!!!				" , {"Ok"} , 2 )	
		Return
	endif

EndIf
Return


//############################//
//	Gera Notas Fiscais        //
//                            //
//############################//
Static Function GeraNF(cDoc)

Local CNUMPED := cDoc
Local APVLNFS := {} 
Local _cSerie := '001'

// POSICIONA NO PEDIDO
DBSELECTAREA("SC5")
SC5->(DBSETORDER(1))
SC5->(DBSEEK(XFILIAL("SC5") + CNUMPED)) 

If !Empty(SC5->C5_NOTA)
	MsgInfo("Pedido faturado com SUCESSO!")
	Return
EndIf

//POSICIONA NO ITEM DO PEDIDO
//DBSELECTAREA("SC6")
//SC6->(DBSETORDER(1))
//SC6->(DBSEEK(XFILIAL("SC6") + CNUMPED))

//------------------------------------------------------------------------//
// GERAçãO DA NOTA FISCAL DE SAíDA                                        //
//------------------------------------------------------------------------//
DBSELECTAREA("SC9")
SC9->(DBSETORDER(1))
SC9->(DBSEEK(XFILIAL("SC9") + SC5->C5_NUM)) // PEDIDO DE VENDA LIBERADO

DBSELECTAREA("SC6")
SC6->(DBSETORDER(1))
SC6->(DBSEEK(XFILIAL("SC6") + SC5->C5_NUM + SC9->C9_ITEM)) // ITEM DO PEDIDO

DBSELECTAREA("SE4")
SE4->(DBSETORDER(1))
SE4->(DBSEEK(XFILIAL("SE4") + SC5->C5_CONDPAG)) // CONDIÇÃO DE PAGAMENTO
//SE4->(DBSEEK(XFILIAL("SE4") + SC5->C5_CONDPAG)) // CONDIÇÃO DE PAGAMENTO

DBSELECTAREA("SB1")
SB1->(DBSETORDER(1))
SB1->(DBSEEK(XFILIAL("SB1") + SC9->C9_PRODUTO)) // PRODUTO
//SB1->(DBSEEK(XFILIAL("SB1") + SC9->C9_PRODUTO)) // PRODUTO

DBSELECTAREA("SB2")
SB2->(DBSETORDER(1))
SB2->(DBSEEK(XFILIAL("SB2") + SC9->C9_PRODUTO + SC9->C9_LOCAL)) // SALDO DO PRODUTO

DBSELECTAREA("SF4")
SF4->(DBSETORDER(1))
SF4->(DBSEEK(XFILIAL("SF4") + SC6->C6_TES)) // TES DO PRODUTO

While SC9->(!EOF()) .AND. SC9->C9_PEDIDO == CNUMPED
	IncProc("Processando Pedido de Venda: "+CNUMPED+ "")	
	If Empty(SC9->C9_NFISCAL)
		
		DBSELECTAREA("SC6")
		SC6->(DBSETORDER(1))
		SC6->(DBSEEK(XFILIAL("SC6") + SC5->C5_NUM + SC9->C9_ITEM)) // ITEM DO PEDIDO
		
		AADD( APVLNFS , { 	SC9->C9_PEDIDO		,; // PEDIDO
		SC9->C9_ITEM      	,; // ITEM
		SC9->C9_SEQUEN    	,; // SEQUêNCIA
		SC9->C9_QTDLIB    	,; // QUANTIDADE LIBERADA
		SC9->C9_PRCVEN    	,; // PREçO DE VENDA
		SC9->C9_PRODUTO   	,; // CóDIGO DO PRODUTO
		SF4->F4_ISS == "N"	,;
		SC9->(RECNO())    	,;
		SC5->(RECNO())    	,;
		SC6->(RECNO())    	,;
		SE4->(RECNO())    	,;
		SB1->(RECNO())    	,;
		SB2->(RECNO())    	,;
		SF4->(RECNO())    	,;
		SB2->B2_LOCAL     	})
		
		SC6->(DBSKIP())
	Endif
	
	SC9->(DBSKIP())
	
Enddo

//Caso de erro na rotina verificar parametro "Sugere Qtde Liber.?" da pergunta MTA410 do pedido de venda que deve estar SIM
CNOTA := MAPVLNFS( APVLNFS , _cSerie , .F. , .F. , .F. , .F. , .F. , 0   , 0   , .F. , .F. )

If  Empty(CNOTA)
	Alert("Não foi possível gerar a nota do pedido: "+CNUMPED)
	DISARMTRANSACTION( )
Else
	Aviso("DTFTRFIL() - Atenção" , "Pedido Venda: "+CNUMPED+", Faturado na Nota Fiscal: "+CNOTA+"!" , {"Ok"} , 2 )
Endif

SC9->(DBCLOSEAREA())
SE1->(DBCLOSEAREA())
SF4->(DBCLOSEAREA())
SE4->(DBCLOSEAREA())
SB1->(DBCLOSEAREA())
SB2->(DBCLOSEAREA())
SC5->(DBCLOSEAREA())
SC6->(DBCLOSEAREA())


Return 


//*********************************
// Criando Perguntas 
//********************************
Static Function CriaSX1()

  Local aAreaPerg:= GetArea() 
  Local aHelpPor := {}
  Local aHelpEsp := {}
  Local aHelpIng := {}
  Local cStringP := ""
  Local cStringE := ""
  Local cStringI := ""
  
  // Incluindo Help Pergunta
  aHelpPor := {}
  Aadd( aHelpPor, "Código da TES do Documento de Saida - Filial de Origem. " )  
  aHelpIng := aHelpEsp := aHelpPor 
  PutSx1(cPerg,"01","TES - Doc.Saida","","","mv_ch1","C",TAMSX3("F4_CODIGO")[1],0,0,"G","","SF4","","","MV_PAR01","","","","","","","","","","","","","","","","",aHelpPor,aHelpIng,aHelpEsp )

   aHelpPor := {}
  Aadd( aHelpPor, "" )  
  aHelpIng := aHelpEsp := aHelpPor 
  PutSx1(cPerg,"02","Produto de?","","","mv_ch2","C",TAMSX3("B1_COD")[1],0,0,"G","","SB1","","","MV_PAR02","","","","","","","","","","","","","","","","",aHelpPor,aHelpIng,aHelpEsp )

  aHelpPor := {}
  Aadd( aHelpPor, "" )  
  aHelpIng := aHelpEsp := aHelpPor 
  PutSx1(cPerg,"03","Produto ate?","","","mv_ch3","C",TAMSX3("B1_COD")[1],0,0,"G","","SB1","","","MV_PAR03","","","","","","","","","","","","","","","","",aHelpPor,aHelpIng,aHelpEsp )

  RestArea(aAreaPerg)

Return
